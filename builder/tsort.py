def toposort(data):
    for k, v in data.items():
        v.discard(k) # Ignore self dependencies
    extra_items_in_deps = reduce(set.union, data.values()) - set(data.keys())
    data.update({item:set() for item in extra_items_in_deps})
    while True:
        ordered = set(item for item,dep in data.items() if not dep)
        if not ordered:
            break
        yield ' '.join(sorted(ordered))
        data = {item: (dep - ordered) for item,dep in data.items()
                if item not in ordered}
    assert not data, "A cyclic dependency exists amongst %r" % data


import sys
args = sys.argv
fdep = args[1]

dep = {}
with open(fdep) as dep_file:
    for line in dep_file:
        elmnt = line.split()
        n = len(elmnt)
        if elmnt != []:
            if n > 0:
                dep[elmnt[0]] = set(elmnt[1:n])
            else:
                 dep[elmnt[0]] = set()

sorted_dep = '\n'.join(toposort(dep))

str = fdep.split('.')
out_file = str[0] + '.tsrt'
fout = open(out_file, 'w')

for line in sorted_dep:
    fout.write(line)
