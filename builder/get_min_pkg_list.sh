#!/bin/bash

while read l; do 
    line=$(grep 'BuildRequires:' ../SPECS/R-$l.spec);
    for depkg in $line; do
	depkg=${depkg/R-/}
	depkg=${depkg%-*}
	res=$(grep ${depkg} todo.pkg)
	if [[ "${res}" == "${depkg}" ]]; then
	    sed -i "s|${depkg}||g" todo.pkg
	fi
    done
done<todo.pkg
