#!/bin/bash
######################################################################
## Filename:      build_full.sh
## Author:        Prasad Maddumage <mhemantha@fsu.edu>
## Created at:    Thu Aug 06 14:11:59 2015
## Modified at:   Sun May 21 09:09:15 2017
## Modified by:   Prasad Maddumage <mhemantha@fsu.edu>
## Version:       
## Description:   Download all dependancies of an R package and find 
##                order of installation. It also creates spec files
##                for each package and plots the dependancy tree
######################################################################

function containsElement () {
  local e
  for e in "${@:2}"; do [[ "$e" == "$1" ]] && return 0; done
  return 1
}

function get_pkg_src {
    pkg=$1
    #Remove "devel" as devel version is provided by the package
    if [ -n "${pkg%*devel*}" ]; then
        pkg=${pkg%-devel}
    fi
    #Some packages have a dash (-) instead of a dot (.). Fix it
    tmp=${pkg//[^-]}
    ndash=${#tmp}
    str=$(echo ${pkg}|cut -d"-" -f2)
    if [ "$ndash" -gt 1 ]; then
	for ((i=3; i <= "$((ndash+1))"; i++)); do
	    str=$str.$(echo ${pkg}|cut -d"-" -f"$i")
	done
    fi
    pkg=$str

    #Handle exceptions to CRAN and Bioconductor first
    #Add other cases as needed in the future
    case ${pkg} in
	"Rcompression")
	    url=http://www.omegahat.org/Rcompression/Rcompression_0.93-2.tar.gz
	    ;;
	"RDCOMClient")
	    url=http://www.omegahat.org/RDCOMClient/RDCOMClient_0.93-0.tar.gz
	    ;;
	"MADAM")
	    url=https://cran.r-project.org/src/contrib/Archive/MADAM/MADAM_1.2.2.tar.gz
	    ;;
	*)
	    #Get the CRAN package info. It is easy if the cran.html already exists
	    if ! [ -a cran.html ]
	    then
		eval wget --quiet https://cran.r-project.org/src/contrib/ -O cran.html
	    fi

	    #If it is available in CRAN
	    if grep -q "\"${pkg}_" cran.html; then
		ver=$(grep ${pkg} cran.html|grep -oP "(?<=\"${pkg}_).*?(?=.tar)")
		ver=$(echo $ver|cut -d" " -f1)
		url=https://cran.r-project.org/src/contrib/${pkg}_${ver}.tar.gz
		#If it is a bioconductor package
	    else
		#bioconductor page has no public index.html
		#So, it is harder to find the download path
		#release (latest) version of bioconductor
		#biocbaseurl=http://bioconductor.org/packages/release/bioc/html
		#Use above line for older versions of Bioconductor (eg 2.13)
		#Use http://bioconductor.org/about/release-announcements/#release-versions
		#as a guide to choose correct versions
		biocbaseurl=http://www.bioconductor.org/packages/3.5/bioc/html
		#eval wget --quiet ${biocbaseurl}/${pkg} -O ${pkg}.html
		eval wget --quiet ${biocbaseurl}/${pkg}.html
		#Proceed if the download is successful
		if [ $? != 0 ]
		then
		    #It may be a data package
		    biocbaseurl=http://bioconductor.org/packages/3.1/data/annotation/html
		    eval wget --quiet ${biocbaseurl}/${pkg}.html
		fi
		if [ $? != 0 ]
		then
		    #It may be a data package
		    biocbaseurl=http://bioconductor.org/packages/3.1/data/experiment/html
		    eval wget --quiet ${biocbaseurl}/${pkg}.html
		fi
		if [ $? == 0 ]
		then
		    #We want the package source
		    line_no=$(grep -n "Package Source" ${pkg}.html|cut -d: -f1)
		    #Grabing the the above line (just in case) and the next
		    text=$(sed -n $line_no,$((line_no + 1))p ${pkg}.html)
		    eval rm ${pkg}.html
		    #The source url is given by href="<path>"> (in html) and 
		    #we want <path>
		    url=$(echo $text|grep -oP "(?<=href\=\").*?(?=\"\>)")
		    #Above path is relative. Getting the full url
		    #long_url=$(curl -Ls -o /dev/null -w %{url_effective} \
			#${biocbaseurl}/${pkg}/)
		    #long_url=${long_url%${pkg}.html}
		    #long_url=$long_url${url}
		    #url=${long_url}
		    url=${biocbaseurl}/${url}
		fi
	    fi
    esac

    #Get the source file name from url
    url_split=(${url//\// })
    n=${#url_split[@]}
    src_file=${url_split[n-1]}

    if [ ! -a ../SOURCES/${src_file} ]; then
	eval wget --quiet -O ../SOURCES/${src_file} ${url}
	if [ $? != 0 ]; then
	    echo "Package $1 not found"
	    exit 1
	fi
    fi
    
}


function get_all_deps {
    mypkg=$1
    if [ !  -a ../SPECS/${mypkg}.spec ]; then
	#Download the source
	get_pkg_src ${mypkg}
	#Generate spec file
	specpath=$(/usr/bin/R2spec --no-suggest --no-check \
	    -s ../SOURCES/${src_file}|sed 's|Spec file writen: ||')
	mv ${specpath} ../SPECS/
    fi

    #In R spec files, first BuildRequires line is "Depends" list, second "Imports"
    #and third "Suggests" which we skip (using -m 2 below)
    #Instead, now using --no-suggest in R2spec to skip Suggests list alltogether
    local line=$(grep 'BuildRequires:' ../SPECS/${mypkg}.spec)

    outdeps="${mypkg}"
    for item in ${line}
    do
	containsElement "${item}" "${comb_lib[@]}"
	if [[ $? == 1 && "${item}" != "" && \
	    "${outdeps}" != *"${item}"* && \
	    "${item}" =~ R-.* ]]; then
	    outdeps+=$(echo -n ' ' ${item} | sed -n '/R\-/,//p')
	fi
	
	if [[ "${item}" != "" && "${item}" =~ R-.* ]]; then  
	    insertver=${item/R-/R-%\{Rver\}-}
	    sed -i "s|${item}|${insertver}|g" ../SPECS/${mypkg}.spec
	fi
    done
    echo -e ${outdeps} >> R_deps.txt

    for deps in ${line}
    do
	#Filter everything but R packages
	dep=$(echo -e ${deps}|sed -n '/R\-/,//p')
	#Remove unnecessary commas
	if [ -n "${dep%*,*}" ]; then
            dep=${dep%,}
	fi
	
	containsElement "${dep}" "${comb_lib[@]}"
	if [[ $? == 1 && "${dep}" != "" && ! -a ../SPECS/${dep}.spec ]]
	then
	    get_all_deps ${dep}
	fi
    done
}


function fix_pkg_name {
    local _pkg=$1
    pkg_name=${_pkg#"R-"}
    pkg_name=${pkg_name/"."/"_"}
    #"graph" is a reserved word in graphviz
    pkg_name=${pkg_name/graph/_graph}
}
    

function make_dot_in {
#Create the input file for graphviz's dot command to generate the dependancy tree plot
    local mypkg=$1

    echo 'digraph G {' > ${Rlib}_deps.in 

    while read ln; do
	read -a libs <<< "$ln"
	n=$(( ${#libs[@]} - 1 ))
	fix_pkg_name ${libs[0]}
	libs[0]=${pkg_name}
	for i in `seq 1 $n`; do
	    fix_pkg_name ${libs[$i]}
	    libs[$i]=${pkg_name}
	    echo ${pkg_name} " -> " ${libs[0]} >> ${Rlib}_deps.in
	done
    done<${mypkg}_deps.txt
    echo "}" >> ${Rlib}_deps.in
}


#Beginning main
#Create the directory structure if doesnt exist
mkdir -p ../BUILD ../BUILDROOT ../RPMS ../SOURCES ../SPECS ../SRPMS

#Following packages are provided by R rpm
xclude_lib=(R-core R-core-devel R-compiler R-devel R-base R-boot \
    R-class R-cluster R-codetools R-datasets \
    R-foreign R-graphics R-grDevices R-grid \
    R-KernSmooth R-lattice R-MASS R-Matrix R-Matrix \
    R-methods R-mgcv R-nlme R-nnet R-parallel \
    R-rpart R-spatial R-splines R-stats R-stats4 \
    R-survival R-tcltk R-tools R-utils)

# Rlib=$1
inst_pkg_file=$1
i=0
declare -a inst_lib

#Combine xclude_lib and inst_lib
comb_lib=( "${xclude_lib[@]}" )

if [ -a R_deps.txt ]; then 
    rm R_deps.txt
fi
touch R_deps.txt

while read pkg_need; do
    pkg_inst="$(echo -e "${pkg_need}" | tr -d '[[:space:]]')"
    Rlib=R-${pkg_need}
    #Download source, create spec file, and find all dependancies of ${Rlib}
    get_all_deps ${Rlib}
done<${inst_pkg_file}

#Topologically sort the dependancies to get the install order
#tsort ${Rlib}_deps.txt > ${Rlib}_deps.tsrt
python tsort.py R_deps.txt

#Making the input file needed by dot to plot the dependancy tree
# make_dot_in ${Rlib}

#Creating the dependancy tree plot
# dot ${Rlib}_deps.in > ${Rlib}_deps.dot

#Convert .dot file to a png
#dot -Tpng -Gsize=11\! -Gdpi=300 -Gorientation=landscape \
#    ${Rlib}_deps.dot -o ${Rlib}_deps.png
